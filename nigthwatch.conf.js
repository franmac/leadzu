// Get Selenium and the drivers
// https://medium.freecodecamp.org/how-to-easily-start-automatically-testing-your-website-8629ea8df04a
// Api: http://nightwatchjs.org/api

var seleniumServer = require('selenium-server')
var chromedriver = require('chromedriver')
var geckodriver = require('geckodriver')

var config = {
  src_folders: [
    // Folders with tests
    'test'
  ],
  output_folder: 'tests/reports', // Where to output the test reports
  selenium: {
    // Information for selenium, such as the location of the drivers ect.
    start_process: true,
    server_path: seleniumServer.path,
    host: 'localhost',
    port: 4444, // Standard selenium port
    cli_args: {
      'webdriver.chrome.driver': chromedriver.path,
      'webdriver.gecko.driver': geckodriver.path
    }
  },
  test_workers: {
    // This allows more then one browser to be opened and tested in at once
    enabled: false,
    workers: 'auto'
  },
  test_settings: {
    default: {
      screenshots: {
        enabled: true,
        path: 'tests/reports'
      },
      log_path: 'tests/reports',
      globals: {
        // How long to wait (in milliseconds) before the test times out
        waitForConditionTimeout: 20000,
        user: 'lalala',
        retryAssertionTimeout: 20000,
        abortOnAssertionFailure: false
      },
      desiredCapabilities: {
        // The default test
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
      // 'skip_testcases_on_fail': false //Para seguir ejecutando test incluso cuando alguno anterior falla
    },
    // Here, we give each of the browsers we want to test in, and their driver configuration
    chromeHeadless: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true,
        chromeOptions: {
          args: ['--headless', '--no-sandbox', '--disable-gpu'],
          w3c: false
        },
        binary: '/Applications/Google Chrome.app'
      }
    },
    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true,
        chromeOptions: {
          w3c: false
        },
        binary: '/Applications/Google Chrome.app'
      }
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true,
        firefoxOptions: {
          args: ['--headless'],
          w3c: false
        },
        binary: '/Applications/Firefox.app'
      }
    },
    safari: {
      desiredCapabilities: {
        browserName: 'safari',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
  }
}

module.exports = config
