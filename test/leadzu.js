var login = 'https://www.leadzu.com/login/'
var userProfile = 'https://www.leadzu.com/user/profile'
var user = ''
var pass = ''

module.exports = {
  'Imputación automática de horas': function (browser) {
    browser
        .url(login, () => {
          let arr = process.argv
          user = arr[arr.length - 2].split('=')[1]
          pass = arr[arr.length - 1].split('=')[1]
          browser
          .setValue('input[id=user]', user)
          .setValue('input[id=pass]', pass)
        })
        .pause(2000)
        .click('input[id=send]')
        .pause(2000)
        .url(userProfile).pause(2000)
        .click('li[aria-controls=tabs-gestion-presencia]').pause(2000)
        .assert.urlEquals('https://www.leadzu.com/user/profile#tabs-gestion-presencia')
        .waitForElementPresent('input[id=hora_real')
        .execute(function () {
          document.querySelector('#info_entrada_hora').value = '08'
          document.querySelector('#info_entrada_minuto').value = '00'

          document.querySelector('#info_salida_hora').value = '15'
          document.querySelector('#info_salida_minuto').value = '00'

          document.querySelector('#hora_real').value = '7'
          document.querySelector('#incidencias').value = ''
        })
        .pause(2000)
        .click('input[id=update_gestion_presencia')
        .pause(2000)
        .end()
  }
}
